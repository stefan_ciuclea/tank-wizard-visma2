﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tanks_and_Wizards_de_la_18
{
    class Vrajitor : Unitate
    {
        #region date membre (variabile)
        public int mana;
        #endregion

        #region metode (functii mmembre)

        public void Mergi(int x1, int y1)
        {
            x = x1;
            y = y1;
            Console.WriteLine("Ingineru a mers la coordonatele {0},{1}",x,y);
        }
        public void Alearga(int x1, int y1)
        {
            x = x1;
            y = y1;
            Console.WriteLine("Ingineru a alergat la coordonatele {0},{1}",x,y);
        }
        public void Ataca(int x1, int y1)
        {
            x = x1;
            y = y1;
            Console.WriteLine("Ingineru a atacat la coordonatele {0},{1}",x,y);
        }
        public void Mori()
        {
           Console.WriteLine("{0} a sucombat",nume);
       }
        #endregion
    }
    
}
