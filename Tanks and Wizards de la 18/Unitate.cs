﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tanks_and_Wizards_de_la_18
{
    class Unitate
    {
        #region date membre (variabile)
        public int x, y;
        public string nume;
        public PictureBox poza = new PictureBox();
        public PictureBox pozaDePeHarta = new PictureBox();
        public int viata;
        #endregion
    }
}
