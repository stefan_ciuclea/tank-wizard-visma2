﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tanks_and_Wizards_de_la_18
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("O sa produc un inginer");
            Inginer inginer = new Inginer();
            inginer.nume = "Dorel";
            inginer.poza.Load("d:\\A STEFI\\A Facultate\\An I\\Semestrul II\\Training Visma\\Imagini_C#\\inginer.png");
            pictureBox1.BackgroundImage = inginer.poza.Image;
            inginer.Mori();
        }

        private void button2_Click(object sender, EventArgs e)
        {
        Console.WriteLine("O sa nasc un tanc");
            Tanc tanc = new Tanc();
            tanc.nume = "Pantzer Dorel";
            tanc.poza.Load("d:\\A STEFI\\A Facultate\\An I\\Semestrul II\\Training Visma\\Imagini_C#\\tanc.jpg");
            pictureBox1.BackgroundImage = tanc.poza.Image;

            tanc.Explodeaza();
        }

        private void button3_Click(object sender, EventArgs e)
        {
        Console.WriteLine("Summoning da wizz");
            Vrajitor vrajitor = new Vrajitor();
            vrajitor.nume = "Harry Plotter";
            vrajitor.poza.Load("d:\\A STEFI\\A Facultate\\An I\\Semestrul II\\Training Visma\\Imagini_C#\\vrajitor.png");
            pictureBox1.BackgroundImage = vrajitor.poza.Image;

            vrajitor.Mori();
        }
    }
}
